```plantuml
@startmindmap
+[#Gold] Programando ordenadores en los 80 y ahora. \n   ¿Qué ha cambiado? 
++[#Azure] ¿Que es un sistema antiguo?
+++_ Es
++++[#Yellow] Es un sistema que ya no este a la venta,\n o los que ya no se fueron actualizando.
+++++_ Caracteristicas. 
++++++[#Pink] Se podria saber a fondo como funcionaba el hardware.
++++++[#Pink] Se Utilizaba a fondo todos los recursos disponibles.
++++++[#Pink] Se Programaba directamenta al hardware
++++++[#Pink] No habia dependecias de las Librerias 
++++++[#Pink] se tenia un control total del tiempo\n por que se sabia cuanto tarda cada instruccion
++++++[#Pink] EL lenguaje a la hora de programar era ensamblador
+++++_ Contras
++++++[#Azure] Solo se podia programar en un lenguaje que era el lenguaje maquina
++++++[#Azure] Falta de portabilidad en el lenguaje ensamblador 
++[#Azure] Diferencias que existen entre como se \nprogramabama antes y como se programa ahora.
+++_ como
++++[#Purple] Las maquinas antiguas funcionan igual que como funcionaban antes \n y las de ahora pueden que en el futuro dejen de servir.
++++[#Purple] una es la potencias que existe actualmete con las maquinas\n a las potencias que existian antes.
++++[#Purple] otra es la memoria que a evolucionado de forma extraordinaria\n nada que ver con lo que era antes.
++++[#Purple] que antes hacian distintas las maquinas antiguas\n ya que habia computadoras con el mismo cpu pero eran distintas.
++++[#Purple] otra es el lenguaje antes casi siempre se usaba el lenguaje ensamblador\n y ahora al haber lenguajes de alto nivel se puede programar en distintos lenguajes.
++++[#Purple] Gracias a los lenguajes de alto nivel se hacen\n software mas complejos
++++[#Purple] A comparacion de antes hoy en dia las arquitecturas\n de los ordenadores son muy diferentes a como era antes.
++++[#Purple] Antes el programador conocia cada plataforma\n y ahora es el ordenador el que hace el trabajo. 
++[#Azure] ¿Que es un sistema actual?
+++_ Es
++++[#red] Es el que tenemos en casa o el\n que se pueda comprar en una tienda.
+++++_ caracteristicas 
++++++[#grey] Tiene una gran potencia
++++++[#grey] ahora los sitemas actuales si se llegan a diferenciar,\n un android si diferencia mucho del ios asi como de un ordenador de mesa.
++++++[#grey] y con las aplicaciones para programar uno \n ya no programa directamente en el ordenador.
+++++_ desventajas 
++++++[#Orange] Con la abtraccion que hay hoy dia,\n ya no se conoce a fondo el hardaware
++++++[#Orange] Despendencia del codigo ya que no sabes\n como funciona o como exite.
++++++[#Orange] Sobre carga de recursos ya que al programar\n se llaman a una infinidad de librerias.
++++++[#Orange] Se vuelve mas lento pasando el tiempo.
@endmindmap
```


```plantuml
@startmindmap
+[#Orange] Hª de los algoritmos y de los lenguajes de programación
++[#White] Que es el Programa
+++_ Es 
++++[#Red] Es el conjunto de Operaciones especificadas en un determinado\n lenguaje de Programacion y para un programador completo.
+++++_ Tienen
++++++[#Orange] Lenguajes donde se Programan los algortimos
+++++++_ como
++++++++[#Azure] Simula 
++++++++[#Azure] Java 
++++++++[#Azure] c++
++++++++[#Azure] prolog 
++++++++[#Azure] c 
++++++[#yellow] Lenguajes Logicos
+++++++[#Green] Los Lenguajes logicos se utilizan mas en los problemas de optimizacion 
+++++++[#Green] Tambien se usan en el mundo de la inteligencia artificial 
+++++++[#Green] Las reglas se aplican a los hechos para así obtener conocimiento sobre el medio ambiente.
++++++[#Gold] Lenguajes Funcionales
+++++++  tiene una exelente capacida para crear procesos paralelos distribuidos
+++++++[#Green] En los Chats de algunas aplicaciones 
+++++++[#Green] EN los Bancos 
++++++[#Brown] aportan
+++++++[#white] que los algoritmos se escriben mucho menos lineas, que lleva a un menor numero de errores 
++++++[#Brown] claves para mejorar en la fiabilidad
+++++++[#white] mayor formacion matematica en los ingenieros
+++++++[#white] lenguajes de mas alto nivel 
+++++++[#white] mejores herramientas por ejemplo compiladores
++[#Azure] Que es un algoritmo
+++_ Es 
++++[#Red] Una secuencia ordenada, finita y definida de instrucciones.
+++++_ tipos de algoritmos:
++++++[#Azure] P que son los razonables, son los tipos de problemas que se pueden resolver en tiempo polinomial.
++++++[#Azure] np Son el conjunto de problemas que se facilmente se pordrian resolver con un  si o un no.
+++++_ caracteristicas: 
++++++[#Pink] Al igual que que los problemas matematicas reciben una entrada y entregan una salida
++++++[#Pink] No todos los algoritmos se pueden computar.
++++++[#Pink] En el computador solo se hacen cosas mecanicas 
++++++[#Pink] Un algorimo no puede ser probado hasta que se implementa.
+++++_ Ejemplos de Algoritmos 
++++++[#Brown] como calcular la multiplicacion de dos numeros de muchas cifras
++++++[#brown] Una Receta de cocina.
++++++[#brown] Manuales de ususario.
+++++[#blue] Diferencia de los dispositivos actuales\n con los de antes para calcular los algoritmos
++++++_ por otra parte
+++++++[#Azure] un algoritmo no puede ejecutarse hasta que se implementa
+++++++[#Azure] La Cantidad de Memoria antes no habia tanta memoria las cuales\n se usan para los grandes algoritmos que ocupan muchos datos
@endmindmap
```

```plantuml
@startmindmap
+[#Yellow] Tendencias actuales en los lenguajes de Programación y Sistemas Informáticos.\n La evolución de los lenguajes y paradigmas de programación 
++_ los lenguajes son
+++ el medio de comunicacion entre el hombre y la maquina
++++_ tienen
+++++ caracteristicas 
++++++ Sintaxis el conjunto de símbolos y reglas para formar sentencias.
++++++ Semántica las reglas para transformar sentencias en instrucciones lógicas.
++++++ Pragmática utilizando las construcciones particulares del lenguaje.
+++++ tipos 
++++++_ como
+++++++ c
+++++++ c++
+++++++ java
+++++++ Php
+++++++ Python
+++++++ Visual Basic. 
++_ El paradigma Es
+++[#White] la forma de acercarse a la solucion de un problema.
++++[#Red] Objetivo
+++++_ como 
++++++[#Blue] Encontrar Paradigmas mas abstractos para acercarse mas a nuestro lenguaje.
++++[#White] Tipos de paradigma
+++++_ como
++++++[#Orange] Orientada a Objetos 
+++++++[#Red] Reutiliza el código y evita su duplicación, ademas de Encapsula la información. 
++++++[#Orange] Estructurado 
+++++++[#Red]  ejecución de una sentencia tras otra 
++++++[#Orange] Funcional
+++++++[#Red] Todas las sentencias son en sentido matematico
++++++[#Orange] Logico 
+++++++[#Red] se cuenta con la recursividad no se cuenta con numeros ni letras.
++++++[#Orange] orientada a Aspectos
+++++++[#Red] permite una adecuada modularización de las aplicaciones y posibilita una mejor separación de responsabilidades
++++ Tipos de Programacion
+++++_ son
++++++[#Grey] distribuida 
+++++++[#red] facilidad  de compartir informacion y el trabajo a distancia
++++++[#Grey] concurrente
+++++++[#red]  es una manera de dar solucion a si una multitud de usuarios acceden al programa
++++++[#Grey] Basada en Componentes
+++++++[#red]  Se basa en la implementacion de sistemas utilizando componentes previamente programados y probados 
++++_ caracteristicas 
+++++[#Green] crear sistemas más complejos
+++++[#Green] Facilita y hace más rápido el desarrollo de aplicaciones 
+++++[#Green] Los programas son más fáciles de entender, pueden ser leídos de forma secuencial.
+++++[#Green] Proporciona conceptos y herramientas con las cuales se modela \ny representa el mundo real tan fielmente como sea posible.
@endmindmap
```
